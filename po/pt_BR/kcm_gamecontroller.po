# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-11-14 14:00-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: axesmodel.cpp:63
#, kde-format
msgctxt "@label Axis value"
msgid "Value"
msgstr "Valor"

#: buttonmodel.cpp:74
#, kde-format
msgctxt "Status of a gamepad button"
msgid "PRESSED"
msgstr ""

#: buttonmodel.cpp:84
#, kde-format
msgctxt "@label Button state"
msgid "State"
msgstr "Estado"

#: devicemodel.cpp:51
#, kde-format
msgctxt "Device name and path"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: ui/main.qml:22
#, kde-format
msgid "No game controllers found"
msgstr "Nenhum controlador de jogo encontrado"

#: ui/main.qml:54
#, kde-format
msgctxt "@label:textbox"
msgid "Device:"
msgstr "Dispositivo:"

#: ui/main.qml:81
#, kde-format
msgctxt "@label Visual representation of an axis position"
msgid "Position:"
msgstr "Posição:"

#: ui/main.qml:100
#, kde-format
msgctxt "@label Gamepad buttons"
msgid "Buttons:"
msgstr "Botões:"

#: ui/main.qml:122
#, kde-format
msgctxt "@label Gamepad axes (sticks)"
msgid "Axes:"
msgstr "Eixos:"
